package com.luxoft.dnepr.courses.toprank;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;


public class TopRankSearcher {

    private static final double DEFAULT_DAMPING_FACTOR = 0.8;
    private static final int DEFAULT_NUMBER_OF_LOOPS_IN_RANK_COMPUTING = 10;
    private static final int THREADS_COUNT = 4;


    private String getSiteContent(String siteUrl) {
        System.setProperty("java.net.useSystemProxies", "true");
        try {
            URL url = URI.create(siteUrl).toURL();
            URLConnection connection;
            connection = url.openConnection();
            connection.connect();
            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer stringBuffer = new StringBuffer();
            while ((inputLine = br.readLine()) != null) {
                stringBuffer.append(inputLine);
            }
            return stringBuffer.toString();
        } catch (MalformedURLException e) {
            //e.printStackTrace();
        } catch (IOException e) {
            //e.printStackTrace();
        }
        return "";
    }


    private Map<String, String> getUrlContent(List<String> urls) {
        final Map<String, String> urlContent = new ConcurrentHashMap<>();
        ExecutorService threadPool = Executors.newFixedThreadPool(THREADS_COUNT);
        for (String url : urls) {
            final String tmpUrl = url;
            threadPool.submit(new Runnable() {
                public void run() {
                    urlContent.put(tmpUrl, getSiteContent(tmpUrl));
                }
            });
        }
        threadPool.shutdown();
        try {
            threadPool.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
        }
        return urlContent;
    }


    public TopRankResults execute(List<String> urls) {
        TopRankExecutor topRankExecutor = new TopRankExecutor(DEFAULT_DAMPING_FACTOR, DEFAULT_NUMBER_OF_LOOPS_IN_RANK_COMPUTING);
        return topRankExecutor.execute(getUrlContent(urls));
    }


    public TopRankResults execute(List<String> urls, double dampingFactor, int numberOfLoopsInRankComputing) {
        TopRankExecutor topRankExecutor = new TopRankExecutor(dampingFactor, numberOfLoopsInRankComputing);
        return topRankExecutor.execute(getUrlContent(urls));
    }

}
