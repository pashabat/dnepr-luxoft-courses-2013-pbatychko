package com.luxoft.dnepr.courses.toprank;


import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;


public class TopRankExecutor {

    private static final int THREADS_COUNT = 4;

    private double dampingFactor;
    private int numberOfLoopsInRankComputing;


    public TopRankExecutor(double dampingFactor, int numberOfLoopsInRankComputing) {
        this.dampingFactor = dampingFactor;
        this.numberOfLoopsInRankComputing = numberOfLoopsInRankComputing;
    }


    private Map<String, List<String>> generateReverseGraph(final Map<String, String> urlContent) {
        final Map<String, List<String>> uniqueLinks = new HashMap();

        ExecutorService threadPool = Executors.newFixedThreadPool(THREADS_COUNT);

        Iterator iterator = urlContent.entrySet().iterator();
        while (iterator.hasNext()) {
            final Map.Entry<String, String> site = (Map.Entry) iterator.next();
            final List<String> pageLinks = new ArrayList<String>();

            threadPool.submit(new Runnable() {
                public void run() {
                    for (String key : urlContent.keySet()) {
                        if (!key.equals(site.getKey()) && site.getValue().contains(key)) pageLinks.add(key);
                    }
                    uniqueLinks.put(site.getKey(), new ArrayList<String>(pageLinks));
                }
            });
        }

        threadPool.shutdown();
        try {
            threadPool.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
        }

        return uniqueLinks;
    }


    private Map<String, List<String>> generateGraph(final Map<String, String> urlContent) {
        final Map<String, List<String>> uniqueLinks = new HashMap();

        ExecutorService threadPool = Executors.newFixedThreadPool(THREADS_COUNT);

        Iterator iterator = urlContent.entrySet().iterator();
        while (iterator.hasNext()) {
            final List<String> pageLinks = new ArrayList<String>();
            final Map.Entry<String, String> site = (Map.Entry) iterator.next();

            threadPool.submit(new Runnable() {
                public void run() {
                    for (String key : urlContent.keySet()) {
                        if (!key.equals(site.getKey()) && urlContent.get(key).toString().contains(site.getKey())) pageLinks.add(key);
                    }
                    uniqueLinks.put(site.getKey(), new ArrayList<String>(pageLinks));
                }
            });
        }

        threadPool.shutdown();
        try {
            threadPool.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
        }

        return uniqueLinks;
    }


    private Map<String, Double> setDefaultRanks(Map<String, String> urlContent) {
        Map<String, Double> ranks = new HashMap();
        Double rating = 1.0 / urlContent.size();
        for (String site : urlContent.keySet()) {
            ranks.put(site, rating);
        }
        return ranks;
    }


    private Map<String, Double> generateRanks(Map<String, String> urlContent, Map<String, List<String>> graph) {
        Map<String, Double> ranks = setDefaultRanks(urlContent);
        Double x = (1 - dampingFactor) / urlContent.size();

        for (int i = 0; i < numberOfLoopsInRankComputing; i++) {
            for (String site : graph.keySet()) {
                Double sum = 0.0;
                for (Map.Entry<String, List<String>> tmp : graph.entrySet()) {
                    if (!tmp.getKey().equals(site) && tmp.getValue().contains(site)) {
                        sum += dampingFactor * ranks.get(tmp.getKey()) / tmp.getValue().size();
                    }
                }
                Double rank = x + sum;
                ranks.put(site, rank);
            }
        }
        return ranks;
    }


    private Map<String, List<String>> generateIndex(final Map<String, String> urlContent) {
        final Map<String, List<String>> index = new HashMap<String, List<String>>();
        ExecutorService threadPool = Executors.newFixedThreadPool(THREADS_COUNT);

        for (Map.Entry<String, String> site : urlContent.entrySet()) {
            final String[] words = site.getValue().split("[\t\n\\s]");
            final Map.Entry<String, String> siteFinal = site;
            threadPool.submit(new Runnable() {
                public void run() {
                    for (String word : words) {
                        if (!index.containsKey(word)) {
                            ArrayList<String> tmp = new ArrayList<String>();
                            tmp.add(siteFinal.getKey());
                            index.put(word, tmp);
                        }
                        else if(!index.get(word).contains(siteFinal.getKey())) {
                            index.get(word).add(siteFinal.getKey());
                        }
                    }
                }
            });
        }

        threadPool.shutdown();
        try {
            threadPool.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
        }

        return index;
    }


    public TopRankResults execute(Map<String, String> urlContent) {
        TopRankResults results = new TopRankResults();

        results.setGraph(generateGraph(urlContent));
        results.setReverseGraph(generateReverseGraph(urlContent));
        results.setRanks(generateRanks(urlContent, results.getReverseGraph()));
        results.setIndex(generateIndex(urlContent));

        return results;
    }
}
