<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="unit17.controller.JspFunctions" %>
<%@ page import="unit17.model.Constants" %>

<html>
<head>
    <title></title>
</head>
<body>
Active sessions <%= JspFunctions.getAdminStatistic(request).get(Constants.ACTIVE_SESSION_ATTRIBUTE) %><br/>
Active sessions (ROLE user) <%= JspFunctions.getAdminStatistic(request).get("user") %><br/>
Active sessions (ROLE admin) <%= JspFunctions.getAdminStatistic(request).get("admin") %><br/>
Total count of HttpRequests <%= JspFunctions.getAdminStatistic(request).get("httpCount") %><br/>
Total count of POST HttpRequests <%= JspFunctions.getAdminStatistic(request).get("post") %><br/>
Total count of GET HttpRequests <%= JspFunctions.getAdminStatistic(request).get("get") %><br/>
Total count of Other HttpRequests <%= JspFunctions.getAdminStatistic(request).get("other") %><br/>
</body>
</html>