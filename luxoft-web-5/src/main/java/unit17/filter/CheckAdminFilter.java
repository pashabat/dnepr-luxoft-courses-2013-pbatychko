package unit17.filter;


import unit17.controller.UserClass;
import unit17.model.Constants;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class CheckAdminFilter implements Filter {

    public void init(FilterConfig filterConfig) throws ServletException {
    }


    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        UserClass user = new UserClass(request);
        if (user.hasAccess()) {
            filterChain.doFilter(request, response);
        } else {
            response.sendRedirect(request.getServletContext().getContextPath() + Constants.USER_PAGE);
            return;
        }
    }


    public void destroy() {
    }

}
