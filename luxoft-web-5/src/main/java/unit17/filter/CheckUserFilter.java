package unit17.filter;


import unit17.controller.UserClass;
import unit17.model.Constants;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class CheckUserFilter implements Filter {

    public void init(FilterConfig filterConfig) throws ServletException {
    }


    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        UserClass user = new UserClass(request);
        if (user.isLogin()) {
            filterChain.doFilter(request, response);
        } else {
            response.addCookie(new Cookie(Constants.COOKIE_LOGIN_ERROR, "1"));
            response.sendRedirect(request.getServletContext().getContextPath() + Constants.MAIN_PAGE);
            return;
        }
    }


    public void destroy() {
    }

}
