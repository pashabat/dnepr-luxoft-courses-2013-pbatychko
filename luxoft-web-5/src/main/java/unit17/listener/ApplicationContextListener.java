package unit17.listener;



import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import unit17.model.Constants;
import unit17.model.UserStructure;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

public class ApplicationContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        sce.getServletContext().setAttribute(Constants.ACTIVE_SESSION_ATTRIBUTE, new AtomicLong(0));

        // get users list from ServletContext
        Gson gson = new Gson();
        List<UserStructure> usersList = gson.fromJson(sce.getServletContext().getInitParameter("users")
                , new TypeToken<ArrayList<UserStructure>>(){}.getType());
        sce.getServletContext().setAttribute(Constants.CONTEXT_ALL_USERS, usersList);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        sce.getServletContext().removeAttribute(Constants.ACTIVE_SESSION_ATTRIBUTE);
    }
}
