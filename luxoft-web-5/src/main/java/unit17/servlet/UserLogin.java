package unit17.servlet;



import unit17.controller.UserClass;
import unit17.model.Constants;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;


public class UserLogin extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        UserClass user = new UserClass(request);

        if (user.userLogin()) {
            if (user.getUser().getRole().equals("user")) Constants.ACTIVE_USER_SESSION.getAndIncrement();
            else if (user.getUser().getRole().equals("admin")) Constants.ACTIVE_ADMIN_SESSION.getAndIncrement();

            response.addCookie(new Cookie(Constants.COOKIE_LOGIN_ERROR, "0"));

            HttpSession session = request.getSession();
            session.setAttribute(Constants.USER_SESSION_LOGIN, user.getUser().getLogin());
            session.setAttribute(Constants.USER_SESSION_ROLE, user.getUser().getRole());

            response.sendRedirect(request.getServletContext().getContextPath() + Constants.USER_PAGE);
            return;
        } else {
            response.addCookie(new Cookie(Constants.COOKIE_LOGIN_ERROR, "1"));
            response.sendRedirect(request.getServletContext().getContextPath() + Constants.MAIN_PAGE);
            return;
        }
    }


}
