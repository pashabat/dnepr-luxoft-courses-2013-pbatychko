package unit17.controller;


import unit17.model.Constants;
import unit17.model.UserStructure;

import javax.servlet.http.HttpServletRequest;

import java.util.ArrayList;
import java.util.List;


public class UserClass {

    private UserStructure user;
    private HttpServletRequest request;



    public UserClass(HttpServletRequest request) {
        this.request = request;
    }


    public boolean userLogin() {
        if (isUserInUsersList(request.getParameter("name")) && user.getPassword().equals(request.getParameter("pass"))) {
            return true;
        }
        return false;
    }


    public boolean isLogin() {
        Object login = request.getSession().getAttribute(Constants.USER_SESSION_LOGIN);
        if (login != null && isUserInUsersList(login.toString())) {
            return true;
        }
        return false;
    }


    private boolean isUserInUsersList(String name) {
        if (name != null && name.length() > 0) {
            List<UserStructure> usersList = (ArrayList) request.getServletContext().getAttribute(Constants.CONTEXT_ALL_USERS);
            for (UserStructure user : usersList) {
                if (user.getLogin().equals(name)) {
                    this.user = new UserStructure(user);
                    return true;
                }
            }
        }
        return false;
    }


    public UserStructure getUser() {
        return user;
    }


    public boolean hasAccess() {
        if (isLogin() && user.getRole().equals("admin")) {
            return true;
        }
        return false;
    }

}
