package unit17.model;


import java.util.concurrent.atomic.AtomicLong;


public interface Constants {

    String CONTEXT_ALL_USERS = "allUsers";

    AtomicLong HTTP_POST_REQUEST_COUNTER = new AtomicLong(0);
    AtomicLong HTTP_GET_REQUEST_COUNTER = new AtomicLong(0);
    AtomicLong HTTP_OTHER_REQUEST_COUNTER = new AtomicLong(0);

    String USER_SESSION_LOGIN = "user_login";
    String USER_SESSION_ROLE = "user_role";
    String COOKIE_LOGIN_ERROR = "user_login_error";   // 1 - error, 0 - ok

    String ACTIVE_SESSION_ATTRIBUTE = "ACTIVE_SESSION";

    String USER_PAGE = String.format("/user");
    String MAIN_PAGE = String.format("/index.html");
    String LOGOUT_PAGE = String.format("/logout");

    AtomicLong ACTIVE_USER_SESSION = new AtomicLong(0);
    AtomicLong ACTIVE_ADMIN_SESSION = new AtomicLong(0);

}
