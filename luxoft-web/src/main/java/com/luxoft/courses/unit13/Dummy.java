package com.luxoft.courses.unit13;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

public class Dummy extends HttpServlet {

    private static Map<String, Integer> people = new HashMap<>();
    private static final String NAME = "name";
    private static final String AGE = "age";
    private static final String CONTENT_TYPE = "application/json; charset=utf-8";


    protected void doPut(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter writer = response.getWriter();
        StringBuffer stringBuffer = new StringBuffer();

        synchronized (people) {
            if (request.getParameter(NAME).equals(null) || request.getParameter(NAME).length() == 0 || request.getParameter(AGE).equals(null) || request.getParameter(AGE).length() == 0) {
                response.setStatus(500);
                response.setContentType(CONTENT_TYPE);
                stringBuffer.append("{\"error\": \"Illegal parameters\"}");
                response.setContentLength(stringBuffer.length());
            }
            else if (people.containsKey(request.getParameter(NAME))) {
                response.setStatus(500);
                response.setContentType(CONTENT_TYPE);
                stringBuffer.append("{\"error\": \"Name ");
                stringBuffer.append(request.getParameter(NAME));
                stringBuffer.append(" already exists\"}");
                response.setContentLength(stringBuffer.length());
            }
            else {
                people.put(request.getParameter(NAME), Integer.parseInt(request.getParameter(AGE)));
                response.setStatus(201);
            }
        }

        writer.print(stringBuffer);
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter writer = response.getWriter();
        StringBuffer stringBuffer = new StringBuffer();

        synchronized (people) {
            if (request.getParameter(NAME).equals(null) || request.getParameter(NAME).length() == 0 || request.getParameter(AGE).equals(null) || request.getParameter(AGE).length() == 0) {
                response.setStatus(500);
                response.setContentType(CONTENT_TYPE);
                stringBuffer.append("{\"error\": \"Illegal parameters\"}");
                response.setContentLength(stringBuffer.length());
            }
            else if (!people.containsKey(request.getParameter(NAME))) {
                response.setStatus(500);
                response.setContentType(CONTENT_TYPE);
                stringBuffer.append("{\"error\": \"Name ");
                stringBuffer.append(request.getParameter(NAME));
                stringBuffer.append(" does not exist\"}");
                response.setContentLength(stringBuffer.length());
            }
            else {
                people.put(request.getParameter(NAME), Integer.parseInt(request.getParameter(AGE)));
                response.setStatus(202);
            }
        }

        writer.print(stringBuffer);
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        doPut(request, response);

        response.setContentType("text/html");
        PrintWriter writer = response.getWriter();
        StringBuffer stringBuffer = new StringBuffer();

        for (Map.Entry<String, Integer> person : people.entrySet()) {
            stringBuffer.append(person.getKey());
            stringBuffer.append(" : ");
            stringBuffer.append(person.getValue());
            stringBuffer.append("<br/>");
        }

        writer.print(stringBuffer);
    }

}
