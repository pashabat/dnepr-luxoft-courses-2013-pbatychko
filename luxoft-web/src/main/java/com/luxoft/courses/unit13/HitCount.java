package com.luxoft.courses.unit13;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


public class HitCount extends HttpServlet {

    private static Integer hitCount = 0;


    public Integer getHitCount() {
        synchronized ((Object)hitCount) {
            hitCount++;
            return hitCount;
        }
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter writer = response.getWriter();

        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("{\"hitCount\": ");
        stringBuffer.append(getHitCount().toString());
        stringBuffer.append("}");

        response.setStatus(200);
        response.setContentType("application/json; charset=utf-8");
        response.setContentLength(stringBuffer.length());
        writer.print(stringBuffer);
    }
}
