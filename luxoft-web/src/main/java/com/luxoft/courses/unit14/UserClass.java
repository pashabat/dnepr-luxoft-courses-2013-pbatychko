package com.luxoft.courses.unit14;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;


public class UserClass {

    private String name = "";
    private String pass = "";
    private Map<String, String> allUsers = new HashMap<>();
    private HttpServletRequest request;
    private HttpServletResponse response;
    private String servletContextUsers;

    public static String COOKIE_NAME = "user_name";
    public static String COOKIE_LOGIN_ERROR = "user_login_error";   // 1 - error, 0 - ok
    public static String CONTEXT_USERS_LIST_TITLE = "users";


    public UserClass(HttpServletRequest request, HttpServletResponse response, String servletContextUsers) {
        this.request = request;
        this.response = response;
        this.servletContextUsers = servletContextUsers;
    }


    public boolean userLogin() {
        name = request.getParameter("name");
        pass = request.getParameter("pass");
        getUsersList();
        if (allUsers.containsKey(name) && allUsers.get(name).equals(pass)) return true;
        return false;
    }


    public boolean isLogin() {
        getUsersList();
        Cookie[] cookies = request.getCookies();
        for (int i = 0; i < cookies.length; i++) {
            if (COOKIE_NAME.equals(cookies[i].getName()) && allUsers.containsKey(cookies[i].getValue())) {
                name = cookies[i].getValue();
                return true;
            }
        }
        return false;
    }


    private void getUsersList() {
        Gson gson = new Gson();
        allUsers = gson.fromJson(servletContextUsers, new TypeToken<Map<String, String>>(){}.getType());
    }


    public String getName() {
        return name;
    }


    /*public boolean hasAccess() {
        return true;
    } */

}
