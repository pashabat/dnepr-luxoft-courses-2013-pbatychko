package com.luxoft.courses.unit14;


import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


public class UserPage extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        UserClass user = new UserClass(request, response, getServletContext().getInitParameter(UserClass.CONTEXT_USERS_LIST_TITLE));

        if (user.isLogin()) {
            PrintWriter writer = response.getWriter();
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("" +
                    "<!DOCTYPE html>\n" +
                    "<html>\n" +
                    "<head>\n" +
                    "    <title>User page | Unit 14</title>\n" +
                    "    <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />\n" +
                    "    <link rel='stylesheet' type='text/css' href='css/position.css' />\n" +
                    "    <link rel='stylesheet' type='text/css' href='css/style.css' />\n" +
                    "</head>\n" +
                    "<body>\n" +
                    "    <div class='user-menu'>\n" +
                    "        <a href='/myFirstWebApp/logout'>logout</a>\n" +
                    "    </div>\n" +
                    "    <div class='clear'></div>\n" +
                    "    <div class='user-name'>Hello " + user.getName() + "!</div>\n" +
                    "</body>\n" +
                    "</html>");
            writer.print(stringBuffer);
        }
        else {
            response.addCookie(new Cookie(UserClass.COOKIE_LOGIN_ERROR, "1"));
            response.sendRedirect("/myFirstWebApp/index.html");
            return;
        }
    }

}
