package com.luxoft.courses.unit14;


import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class UserLogin extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        UserClass user = new UserClass(request, response, getServletContext().getInitParameter(UserClass.CONTEXT_USERS_LIST_TITLE));

        if(user.userLogin()) {
            response.addCookie(new Cookie(UserClass.COOKIE_NAME, user.getName()));
            response.addCookie(new Cookie(UserClass.COOKIE_LOGIN_ERROR, "0"));
            response.sendRedirect("/myFirstWebApp/user");
            return;
        } else {
            response.addCookie(new Cookie(UserClass.COOKIE_LOGIN_ERROR, "1"));
            response.sendRedirect("/myFirstWebApp/index.html");
            return;
        }
    }

}
