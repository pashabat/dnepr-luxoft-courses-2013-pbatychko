package com.luxoft.courses.unit14;


import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class UserLogout extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.addCookie(new Cookie(UserClass.COOKIE_NAME, ""));
        response.addCookie(new Cookie(UserClass.COOKIE_LOGIN_ERROR, "0"));

        response.sendRedirect("/myFirstWebApp/index.html");
        return;
    }

}
