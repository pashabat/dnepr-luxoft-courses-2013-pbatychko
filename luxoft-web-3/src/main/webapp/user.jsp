<%@ page import="com.luxoft.courses.unit15.controller.JspFunctions" %>
<%@ page import="com.luxoft.courses.unit15.model.Constants" %>


<!DOCTYPE html>
<html>
<head>
    <title>User page | Unit 15</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" type="text/css" href="css/position.css"/>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
</head>
<body>
<div class="user-menu">
    <a href="<%= Constants.LOGOUT_PAGE %>">logout</a>
</div>
<div class="clear"></div>
<div class="user-name">Hello <%= JspFunctions.getUserData(request).getUser().getLogin() %>!</div>
</body>
</html>