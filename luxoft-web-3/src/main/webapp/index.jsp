<!DOCTYPE html>
<html>
<head>
    <title>Unit 15</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <link rel="stylesheet" type="text/css" href="css/position.css"/>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>

    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/jquery_cookie.js"></script>
    <script type="text/javascript" src="js/show_error.js"></script>

</head>


<body>
<div class="enter">
    <form action="/luxoft-web-3/userLogin" method="post">
        <div class="field">
            <div class="field-title">Login:</div>
            <div class="field-input"><input type="text" name="name"></div>
            <div class="error">Wrong login or password</div>
            <div class="clear"></div>
        </div>

        <div class="field">
            <div class="field-title">Password:</div>
            <div class="field-input"><input type="password" name="pass"></div>
            <div class="clear"></div>
        </div>

        <div class="field">
            <input type="submit" value="Sign In" class="sign-in">
        </div>
    </form>

</div>
</body>
</html>