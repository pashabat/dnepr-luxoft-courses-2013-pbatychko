package com.luxoft.courses.unit15.servlet;


import com.luxoft.courses.unit15.controller.UserClass;
import com.luxoft.courses.unit15.model.Constants;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;


public class UserLogin extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        UserClass user = new UserClass(request);

        if (user.userLogin()) {
            if (user.getUser().getRole().equals("user")) Constants.ACTIVE_USER_SESSION.getAndIncrement();
            else if (user.getUser().getRole().equals("admin")) Constants.ACTIVE_ADMIN_SESSION.getAndIncrement();

            response.addCookie(new Cookie(UserClass.COOKIE_LOGIN_ERROR, "0"));

            HttpSession session = request.getSession();
            session.setAttribute(UserClass.SESSION_LOGIN, user.getUser().getLogin());

            response.sendRedirect(Constants.USER_PAGE);
            return;
        } else {
            response.addCookie(new Cookie(UserClass.COOKIE_LOGIN_ERROR, "1"));
            response.sendRedirect(Constants.MAIN_PAGE);
            return;
        }
    }


}
