package com.luxoft.courses.unit15.servlet;


import com.luxoft.courses.unit15.controller.UserClass;
import com.luxoft.courses.unit15.model.Constants;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class UserLogout extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        UserClass user = new UserClass(request);

        if (user.isLogin()) {
            response.addCookie(new Cookie(UserClass.COOKIE_LOGIN_ERROR, "0"));

            if (user.getUser().getRole().equals("user")) Constants.ACTIVE_USER_SESSION.getAndDecrement();
            else if (user.getUser().getRole().equals("admin")) Constants.ACTIVE_ADMIN_SESSION.getAndDecrement();

            if (request.getSession(false) != null) {
                request.getSession(false).invalidate();
            }
        }

        response.sendRedirect(Constants.MAIN_PAGE);
        return;
    }

}
