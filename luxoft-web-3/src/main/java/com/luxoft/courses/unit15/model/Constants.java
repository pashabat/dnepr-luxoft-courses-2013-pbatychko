package com.luxoft.courses.unit15.model;


import java.util.concurrent.atomic.AtomicLong;


public class Constants {

    public static final String ACTIVE_SESSION_ATTRIBUTE = "ACTIVE_SESSION";

    public static AtomicLong HTTP_POST_REQUEST_COUNTER = new AtomicLong(0);
    public static AtomicLong HTTP_GET_REQUEST_COUNTER = new AtomicLong(0);
    public static AtomicLong HTTP_OTHER_REQUEST_COUNTER = new AtomicLong(0);

    public static final String SITE_TITLE = "luxoft-web-3";
    public static final String USER_PAGE = String.format("/%s/user", SITE_TITLE);
    public static final String MAIN_PAGE = String.format("/%s/index.html", SITE_TITLE);
    public static final String LOGOUT_PAGE = String.format("/%s/logout", SITE_TITLE);

    public static AtomicLong ACTIVE_USER_SESSION = new AtomicLong(0);
    public static AtomicLong ACTIVE_ADMIN_SESSION = new AtomicLong(0);

}
