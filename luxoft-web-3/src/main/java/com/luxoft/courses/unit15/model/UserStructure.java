package com.luxoft.courses.unit15.model;


public class UserStructure {

    private String login;
    private String password;
    private String role;


    public UserStructure(UserStructure user) {
        login = user.getLogin();
        password = user.getPassword();
        role = user.getRole();
    }


    public UserStructure(String login, String pass, String role) {
        this.login = login;
        this.password = pass;
        this.role = role;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getRole() {
        return role;
    }
}
