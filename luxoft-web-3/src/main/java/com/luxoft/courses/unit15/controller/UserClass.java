package com.luxoft.courses.unit15.controller;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.luxoft.courses.unit15.model.Constants;
import com.luxoft.courses.unit15.model.UserStructure;

import javax.servlet.ServletContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


public class UserClass {

    private static List<UserStructure> usersList;
    private UserStructure user;
    private HttpServletRequest request;
    private String servletContextUsers;

    public static final String SESSION_LOGIN = "user_login";
    public static final String COOKIE_LOGIN_ERROR = "user_login_error";   // 1 - error, 0 - ok


    public UserClass(HttpServletRequest request) {
        this.request = request;
        this.servletContextUsers = request.getServletContext().getInitParameter("users");
        getUsersList();
    }


    public boolean userLogin() {
        if (isUserInUsersList(request.getParameter("name")) && user.getPassword().equals(request.getParameter("pass"))) {
            return true;
        }
        return false;
    }


    public boolean isLogin() {
        Object login = request.getSession().getAttribute(SESSION_LOGIN);
        if (login != null && isUserInUsersList(login.toString())) {
            return true;
        }
        return false;
    }


    private void getUsersList() {
        if (usersList == null) {
            Gson gson = new Gson();
            usersList = gson.fromJson(servletContextUsers, new TypeToken<CopyOnWriteArrayList<UserStructure>>() {
            }.getType());
        }
    }


    private boolean isUserInUsersList(String name) {
        if (name != null && name.length() > 0) {
            for (UserStructure user : usersList) {
                if (user.getLogin().equals(name)) {
                    this.user = new UserStructure(user);
                    return true;
                }
            }
        }
        return false;
    }


    public UserStructure getUser() {
        return user;
    }


    public boolean hasAccess() {
        if (isLogin() && user.getRole().equals("admin")) {
            return true;
        }
        return false;
    }

}
