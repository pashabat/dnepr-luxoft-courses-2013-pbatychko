package com.luxoft.courses.unit15.controller;


import com.luxoft.courses.unit15.model.Constants;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;


public class JspFunctions {

    public static UserClass getUserData(HttpServletRequest request) {
        UserClass user = new UserClass(request);
        user.isLogin();
        return user;
    }


    public static Map<String, String> getAdminStatistic(HttpServletRequest request) {
        UserClass user = new UserClass(request);
        user.isLogin();
        Map<String, String> adminStatistic = new HashMap<String, String>();

        adminStatistic.put(Constants.ACTIVE_SESSION_ATTRIBUTE, request.getServletContext().getAttribute(Constants.ACTIVE_SESSION_ATTRIBUTE).toString());
        adminStatistic.put("user", ((Long) Constants.ACTIVE_USER_SESSION.get()).toString());
        adminStatistic.put("admin", ((Long) Constants.ACTIVE_ADMIN_SESSION.get()).toString());

        Long httpCount = Constants.HTTP_POST_REQUEST_COUNTER.get() + Constants.HTTP_GET_REQUEST_COUNTER.get() + Constants.HTTP_OTHER_REQUEST_COUNTER.get();
        adminStatistic.put("httpCount", httpCount.toString());
        adminStatistic.put("post", ((Long) Constants.HTTP_POST_REQUEST_COUNTER.get()).toString());
        adminStatistic.put("get", ((Long) Constants.HTTP_GET_REQUEST_COUNTER.get()).toString());
        adminStatistic.put("other", ((Long) Constants.HTTP_OTHER_REQUEST_COUNTER.get()).toString());

        return adminStatistic;
    }
}
