package com.luxoft.courses.unit15.listener;


import com.luxoft.courses.unit15.model.Constants;

import javax.servlet.ServletContext;
import javax.servlet.ServletRequest;
import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.http.HttpServletRequest;


public class CountHttpRequest implements ServletRequestListener {

    public void requestDestroyed(ServletRequestEvent sre) {
    }

    public void requestInitialized(ServletRequestEvent sre) {

        if (sre.getServletRequest() instanceof HttpServletRequest) {
            if (selectRequest(sre, "POST")) {
                Constants.HTTP_POST_REQUEST_COUNTER.getAndIncrement();
            } else if (selectRequest(sre, "GET")) {
                Constants.HTTP_GET_REQUEST_COUNTER.getAndIncrement();
            } else {
                Constants.HTTP_OTHER_REQUEST_COUNTER.getAndIncrement();
            }
        }
    }

    private boolean selectRequest(ServletRequestEvent sre, String requestType) {
        return ((HttpServletRequest) sre.getServletRequest()).getMethod().equals(requestType);
    }
}
