package com.luxoft.courses.unit15.servlet;


import com.luxoft.courses.unit15.controller.UserClass;
import com.luxoft.courses.unit15.model.Constants;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.atomic.AtomicLong;


public class SessionData extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        UserClass user = new UserClass(request);

        if (user.hasAccess()) {
            PrintWriter writer = response.getWriter();
            StringBuffer stringBuffer = new StringBuffer();

            writer.println(String.format("Active sessions %s"
                    , ((AtomicLong) getServletContext().getAttribute(Constants.ACTIVE_SESSION_ATTRIBUTE)).get()));

            writer.println(String.format("Active sessions (ROLE user) %s", Constants.ACTIVE_USER_SESSION.get()));

            writer.println(String.format("Active sessions (ROLE admin) %s", Constants.ACTIVE_ADMIN_SESSION.get()));

            writer.println(String.format("Total count of HttpRequests %s"
                    , (Constants.HTTP_POST_REQUEST_COUNTER.get()
                    + Constants.HTTP_GET_REQUEST_COUNTER.get()
                    + Constants.HTTP_OTHER_REQUEST_COUNTER.get()
            )));

            writer.println(String.format("Total count of POST HttpRequests %s"
                    , Constants.HTTP_POST_REQUEST_COUNTER.get()));

            writer.println(String.format("Total count of GET HttpRequests %s"
                    , Constants.HTTP_GET_REQUEST_COUNTER.get()));

            writer.println(String.format("Total count of Other HttpRequests %s"
                    , Constants.HTTP_OTHER_REQUEST_COUNTER.get()));

        } else if (user.isLogin()) {
            response.sendRedirect(Constants.USER_PAGE);
        } else {
            response.addCookie(new Cookie(UserClass.COOKIE_LOGIN_ERROR, "1"));
            response.sendRedirect(Constants.MAIN_PAGE);
            return;
        }
    }

}