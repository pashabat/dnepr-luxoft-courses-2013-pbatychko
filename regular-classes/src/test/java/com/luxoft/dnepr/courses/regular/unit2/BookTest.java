package com.luxoft.dnepr.courses.regular.unit2;

import org.junit.Test;

import java.util.GregorianCalendar;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;


public class BookTest {
    private ProductFactory productFactory = new ProductFactory();

    @Test
    public void testClone() throws Exception {
        Book book = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Book cloned = (Book) book.clone();
        assertEquals(book.getName(), cloned.getName());
        assertEquals(book.getPublicationDate(), cloned.getPublicationDate());
        assertEquals(book.getCode(), cloned.getCode());
        assertEquals(book.hashCode(), cloned.hashCode());
        assertNotSame(book, cloned);
    }

    @Test
    public void testEquals() throws Exception {
        Book book1 = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Book book2 = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());

        assertEquals(book1.getName(), book2.getName());
        assertEquals(book1.getPublicationDate(), book2.getPublicationDate());
        assertEquals(book1.getCode(), book2.getCode());
        assertEquals(book1.hashCode(), book2.hashCode());

        assertEquals(true, book1.equals(book2));
        assertNotSame(book1, book2);
    }
}
