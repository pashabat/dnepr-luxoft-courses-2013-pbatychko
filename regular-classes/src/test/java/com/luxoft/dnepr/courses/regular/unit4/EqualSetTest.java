package com.luxoft.dnepr.courses.regular.unit4;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;


public class EqualSetTest {

    private EqualSet<String> createTestEqualSet() {
        EqualSet<String> set = new EqualSet<>();
        set.add("1");
        set.add("2");
        set.add("3");
        return set;
    }


    @Test
    public void testAddNullElement() throws Exception {

        EqualSet<String> set = createTestEqualSet();
        assertEquals(3, set.size());

        assertFalse(set.contains(null));

        set.add(null);
        assertEquals(4, set.size());
        set.add(null);
        assertEquals(4, set.size());

        assertTrue(set.contains(null));
    }


    @Test
    public void testRemove() throws Exception {

        EqualSet<String> set = new EqualSet<>(createTestEqualSet());
        assertEquals(3, set.size());

        set.remove("555");
        assertEquals(3, set.size());
        set.remove(null);
        assertEquals(3, set.size());

        set.removeAll(createTestEqualSet());
        assertTrue(set.isEmpty());

        set = createTestEqualSet();
        set.clear();
        assertTrue(set.isEmpty());
    }


    @Test
    public void testContains() throws Exception {

        EqualSet<String> set1 = createTestEqualSet();

        assertTrue(set1.contains("2"));
        assertFalse(set1.contains("5"));

        EqualSet<String> set2 = createTestEqualSet();
        assertTrue(set1.containsAll(set2));
        assertTrue(set2.containsAll(set1));

        set1.add("2");
        assertTrue(set1.containsAll(set2));
        assertTrue(set2.containsAll(set1));

        set1.add("555");
        assertTrue(set1.containsAll(set2));
        assertFalse(set2.containsAll(set1));
    }


    @Test
    public void testRetainAll() throws Exception {

        EqualSet<String> set1 = createTestEqualSet();
        EqualSet<String> set2 = createTestEqualSet();

        assertEquals(3, set1.size());
        assertTrue(set1.equals(set2));
        assertFalse(set1.retainAll(set2));
        assertTrue(set1.equals(set2));

        set1.add("2");
        assertFalse(set2.retainAll(set1));
        assertEquals(4, set1.size());
        assertEquals(3, set2.size());

        assertFalse(set1.retainAll(set2));
        assertEquals(4, set1.size());
        assertEquals(3, set2.size());

        set1.add("5");
        assertFalse(set2.retainAll(set1));
        assertEquals(5, set1.size());
        assertEquals(3, set2.size());

        assertFalse(set1.retainAll(set2));
        assertEquals(4, set1.size());
        assertEquals(3, set2.size());

    }


    @Test
    public void testEquals() throws Exception {

        EqualSet<String> set1 = createTestEqualSet();
        EqualSet<String> set2 = createTestEqualSet();

        assertTrue(set1.equals(set2));
        set1.add("5");
        assertFalse(set1.equals(set2));
        set1.remove("5");
        assertTrue(set1.equals(set2));

        set1.add("2");
        assertFalse(set1.equals(set2));
        set1.remove("2");
        assertFalse(set1.equals(set2));
    }


}
