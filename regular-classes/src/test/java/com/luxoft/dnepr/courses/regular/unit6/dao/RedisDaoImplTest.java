package com.luxoft.dnepr.courses.regular.unit6.dao;


import com.luxoft.dnepr.courses.regular.unit5.dao.RedisDaoImpl;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Redis;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;


public class RedisDaoImplTest {

    private com.luxoft.dnepr.courses.regular.unit5.dao.RedisDaoImpl dao;


    @Before
    public void createEmployee() {
        dao = new RedisDaoImpl();
        try {
            dao.save(new Redis(3, 300));
            dao.save(new Redis(1, 100));
            dao.save(new Redis(2, 200));
        } catch (UserAlreadyExist userAlreadyExist) {
        }
    }


    @Test
    public void testSave() throws Exception {
        assertTrue(dao.get(1).equals(new Redis(1, 100)));
        assertTrue(dao.get(2).equals(new Redis(2, 200)));
        assertTrue(dao.get(3).equals(new Redis(3, 300)));
    }


    @Test(expected = UserAlreadyExist.class)
    public void testSaveUserAlreadyExist() throws Exception {
        dao.save(new Redis(1, 100));
    }


    @Test
    public void testUpdate() throws Exception {
        dao.update(new Redis(2, 500));
        assertTrue(dao.get(2).equals(new Redis(2, 500)));
    }


    @Test(expected = UserNotFound.class)
    public void testUpdateUserNotFound() throws Exception {
        dao.update(new Redis(5, 500));
    }


    @Test
    public void testGet() throws Exception {
        assertEquals(dao.get(10), null);
    }


    @Test
    public void testDelete() throws Exception {
        assertTrue(dao.delete(3));
        assertFalse(dao.delete(3));
    }

}
