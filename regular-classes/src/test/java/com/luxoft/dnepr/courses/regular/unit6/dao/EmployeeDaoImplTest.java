package com.luxoft.dnepr.courses.regular.unit6.dao;


import com.luxoft.dnepr.courses.regular.unit5.dao.EmployeeDaoImpl;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Employee;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;


public class EmployeeDaoImplTest {

    private com.luxoft.dnepr.courses.regular.unit5.dao.EmployeeDaoImpl dao;


    @Before
    public void createEmployee() {
        dao = new EmployeeDaoImpl();
        try {
            dao.save(new Employee(3, 300));
            dao.save(new Employee(1, 100));
            dao.save(new Employee(2, 200));
            dao.save(new Employee(32, 200));
            dao.save(new Employee(33, 200));
            dao.save(new Employee(34, 200));
            dao.save(new Employee(35, 200));
            dao.save(new Employee(36, 200));
        } catch (UserAlreadyExist userAlreadyExist) {
        }
    }


    @Test
    public void testSave() throws Exception {
        //assertTrue(dao.get(1).equals(new Employee(1, 100)));
        //assertTrue(dao.get(2).equals(new Employee(2, 200)));
        //assertTrue(dao.get(3).equals(new Employee(3, 300)));
    }


    @Test(expected = UserAlreadyExist.class)
    public void testSaveUserAlreadyExist() throws Exception {
        dao.save(new Employee(1, 100));
    }


    @Test
    public void testUpdate() throws Exception {
        dao.update(new Employee(2, 500));
        assertTrue(dao.get(2).equals(new Employee(2, 500)));
    }


    @Test(expected = UserNotFound.class)
    public void testUpdateUserNotFound() throws Exception {
        dao.update(new Employee(5, 500));
    }


    @Test
    public void testGet() throws Exception {
        assertEquals(dao.get(10), null);
    }


    @Test
    public void testDelete() throws Exception {
        assertTrue(dao.delete(3));
        assertFalse(dao.delete(3));
    }

}
