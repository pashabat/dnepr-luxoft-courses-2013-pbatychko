package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.NoUserFoundException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.TransactionException;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;


public class BankTest {

    public final static String JAVA_VERSION = "1.7.0_17";


    @Test(expected = IllegalJavaVersionError.class)
    public void testBank() throws IllegalJavaVersionError {
        Bank myBank = new Bank("1.7.0_27");
    }


    @Test(expected = NoUserFoundException.class)
    public void testFindUserById() throws NoUserFoundException, TransactionException {
        Map<Long, UserInterface> users = new HashMap<Long, UserInterface>();
        users.put((long) 1, new User((long) 1, "test user 1", new Wallet((long) 1, BigDecimal.valueOf(1234.50), WalletStatus.ACTIVE, BigDecimal.valueOf(100000.00))));
        users.put((long) 2, new User((long) 2, "test user 2", new Wallet((long) 1, BigDecimal.valueOf(1234.50), WalletStatus.ACTIVE, BigDecimal.valueOf(100000.00))));

        Bank myBank = new Bank(JAVA_VERSION);
        myBank.setUsers(users);
        myBank.makeMoneyTransaction((long) 1, (long) 3, BigDecimal.valueOf(1000.0));
    }


    @Test
    public void testMakeMoneyTransaction() throws Exception {
        Map<Long, UserInterface> users = new HashMap<Long, UserInterface>();

        users.put((long) 1, new User((long) 1, "test user 1", new Wallet((long) 1, BigDecimal.valueOf(500), WalletStatus.ACTIVE, BigDecimal.valueOf(100000.00))));
        users.put((long) 2, new User((long) 2, "test user 2", new Wallet((long) 2, BigDecimal.valueOf(100.10), WalletStatus.ACTIVE, BigDecimal.valueOf(100000.00))));
        users.put((long) 3, new User((long) 3, "test user 3", new Wallet((long) 3, BigDecimal.valueOf(3000.00), WalletStatus.ACTIVE, BigDecimal.valueOf(100000.00))));

        Bank myBank = new Bank(JAVA_VERSION);
        myBank.setUsers(users);
        myBank.makeMoneyTransaction((long) 1, (long) 2, BigDecimal.valueOf(100.90));

        assertEquals(BigDecimal.valueOf(399.1), myBank.getUsers().get((long) 1).getWallet().getAmount());
        assertEquals(BigDecimal.valueOf(201.0), myBank.getUsers().get((long) 2).getWallet().getAmount());
    }

}
