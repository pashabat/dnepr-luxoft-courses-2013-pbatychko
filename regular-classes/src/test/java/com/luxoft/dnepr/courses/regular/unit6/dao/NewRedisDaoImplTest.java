package com.luxoft.dnepr.courses.regular.unit6.dao;

import com.luxoft.dnepr.courses.regular.unit5.dao.IDao;
import com.luxoft.dnepr.courses.regular.unit5.dao.RedisDaoImpl;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Redis;
import com.luxoft.dnepr.courses.regular.unit5.storage.EntityStorage;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class NewRedisDaoImplTest {

    IDao<Redis> dao = new RedisDaoImpl();

    @Before
    public void init() {
        EntityStorage.getEntities().clear();
        EntityStorage.getEntities().put(1L, createRedis(1L, 1));
        EntityStorage.getEntities().put(2L, createRedis(2L, 2));
        EntityStorage.getEntities().put(3L, createRedis(3L, 3));
        EntityStorage.getEntities().put(4L, createRedis(4L, 4));
        EntityStorage.getEntities().put(5L, createRedis(5L, 5));
    }

    @Test
    public void saveBasicTest() throws UserAlreadyExist {
        Redis redis = createRedis(null, 1);
        redis = dao.save(redis);
        Assert.assertEquals(new Long(6), redis.getId());

        redis = createRedis(null, 1);
        redis = dao.save(redis);
        Assert.assertEquals(new Long(7), redis.getId());

        redis = createRedis(null, 1);
        redis = dao.save(redis);
        Assert.assertEquals(new Long(8), redis.getId());
    }

    @Test
    public void saveComplicatedTest() throws UserAlreadyExist {
        Redis redis = createRedis(null, 1);
        redis = dao.save(redis);
        Assert.assertEquals(new Long(6), redis.getId());

        EntityStorage.getEntities().remove(1L);

        redis = createRedis(null, 1);
        redis = dao.save(redis);
        Assert.assertEquals(new Long(7), redis.getId());

        EntityStorage.getEntities().remove(2L);
        EntityStorage.getEntities().remove(3L);

        redis = createRedis(null, 1);
        redis = dao.save(redis);
        Assert.assertEquals(new Long(8), redis.getId());

        EntityStorage.getEntities().remove(7L);
        EntityStorage.getEntities().remove(8L);

        redis = createRedis(null, 1);
        redis = dao.save(redis);
        Assert.assertEquals(new Long(7), redis.getId());
    }

    @Test(expected = UserAlreadyExist.class)
    public void saveAlreadyExistsTest() throws UserAlreadyExist {
        Redis redis = createRedis(1L, 1);
        dao.save(redis);
    }

    @Test(expected = UserAlreadyExist.class)
    public void saveAlreadyExists2Test() throws UserAlreadyExist {
        Redis redis = createRedis(5L, 1);
        dao.save(redis);
    }

    @Test
    public void updateOkTest() throws UserNotFound {
        Redis redis = createRedis(1L, 10);
        redis = dao.update(redis);
        Assert.assertEquals(new Long(1), redis.getId());
        Assert.assertEquals(10, redis.getWeight());
        redis = (Redis) EntityStorage.getEntities().get(1L);
        Assert.assertEquals(new Long(1), redis.getId());
        Assert.assertEquals(10, redis.getWeight());
    }

    @Test(expected = UserNotFound.class)
    public void updateFail1Test() throws UserNotFound {
        Redis redis = createRedis(null, 10);
        redis = dao.update(redis);
    }

    @Test(expected = UserNotFound.class)
    public void updateFail2Test() throws UserNotFound {
        Redis redis = createRedis(6L, 10);
        redis = dao.update(redis);
    }

    @Test
    public void getTest() {
        Redis redis = dao.get(3);
        Assert.assertEquals(new Long(3), redis.getId());
        Assert.assertEquals(3, redis.getWeight());

        redis = dao.get(10);
        Assert.assertNull(redis);
    }

    @Test
    public void deleteTest() {
        Assert.assertFalse(dao.delete(7L));
        Assert.assertFalse(dao.delete(8L));
        Assert.assertFalse(dao.delete(9L));

        Assert.assertTrue(dao.delete(1L));
        Assert.assertTrue(dao.delete(2L));
        Assert.assertTrue(dao.delete(3L));
        Assert.assertTrue(dao.delete(4L));
        Assert.assertTrue(dao.delete(5L));

        Assert.assertFalse(dao.delete(1L));
        Assert.assertFalse(dao.delete(2L));
    }

    private static Redis createRedis(Long id, int weight) {
        Redis redis = new Redis();
        redis.setId(id);
        redis.setWeight(weight);
        return redis;
    }

}
