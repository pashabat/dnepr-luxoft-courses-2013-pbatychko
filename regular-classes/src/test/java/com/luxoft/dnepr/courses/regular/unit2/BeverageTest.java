package com.luxoft.dnepr.courses.regular.unit2;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;


public class BeverageTest {
    private ProductFactory productFactory = new ProductFactory();

    @Test
    public void testClone() throws Exception {
        Beverage beverage = productFactory.createBeverage("beverage1", "Coca-cola", 5, true);
        Beverage cloned = (Beverage) beverage.clone();

        assertEquals(beverage.getName(), cloned.getName());
        assertEquals(beverage.isNonAlcoholic(), cloned.isNonAlcoholic());
        assertEquals(beverage.getCode(), cloned.getCode());
        assertEquals(beverage.hashCode(), cloned.hashCode());
        assertNotSame(beverage, cloned);
    }

    @Test
    public void testEquals() throws Exception {
        Beverage beverage1 = productFactory.createBeverage("beverage1", "Coca-cola", 5, true);
        Beverage beverage2 = productFactory.createBeverage("beverage1", "Coca-cola", 5, true);

        assertEquals(beverage1.getName(), beverage2.getName());
        assertEquals(beverage1.isNonAlcoholic(), beverage2.isNonAlcoholic());
        assertEquals(beverage1.getCode(), beverage2.getCode());
        assertEquals(beverage1.hashCode(), beverage2.hashCode());

        assertEquals(true, beverage1.equals(beverage2));
        assertNotSame(beverage1, beverage2);
    }
}
