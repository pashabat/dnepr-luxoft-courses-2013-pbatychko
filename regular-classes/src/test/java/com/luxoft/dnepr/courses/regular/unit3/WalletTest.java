package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.InsufficientWalletAmountException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.LimitExceededException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.NoUserFoundException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.WalletIsBlockedException;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;


public class WalletTest {


    @Test(expected = WalletIsBlockedException.class)
    public void testCheckWithdrawalWalletIsBlockedException() throws Exception {
        User Pasha = new User((long) 1, "Batychko Pavel", new Wallet((long) 1, BigDecimal.valueOf(1234.50), WalletStatus.BLOCKED, BigDecimal.valueOf(100000.00)));
        Pasha.getWallet().checkWithdrawal(BigDecimal.valueOf(50.50));
    }


    @Test(expected = InsufficientWalletAmountException.class)
    public void testCheckWithdrawalInsufficientWalletAmountException() throws Exception {
        User Pasha = new User((long) 1, "Batychko Pavel", new Wallet((long) 2, BigDecimal.valueOf(1234.50), WalletStatus.ACTIVE, BigDecimal.valueOf(100000.00)));
        Pasha.getWallet().checkWithdrawal(BigDecimal.valueOf(5000.50));
    }

    @Test
    public void testWithdrawal() throws Exception {
        User Pasha = new User((long) 1, "Batychko Pavel", new Wallet((long) 2, BigDecimal.valueOf(1234.50), WalletStatus.ACTIVE, BigDecimal.valueOf(100000.00)));
        Pasha.getWallet().withdraw(BigDecimal.valueOf(234.50));
        assertEquals(BigDecimal.valueOf(1000.0), Pasha.getWallet().getAmount());
    }


    @Test(expected = WalletIsBlockedException.class)
    public void checkTransferWalletIsBlockedException() throws Exception {
        User Pasha = new User((long) 1, "Batychko Pavel", new Wallet((long) 1, BigDecimal.valueOf(1234.50), WalletStatus.BLOCKED, BigDecimal.valueOf(100000.00)));
        Pasha.getWallet().checkTransfer(BigDecimal.valueOf(50.50));
    }

    @Test(expected = LimitExceededException.class)
    public void checkTransferLimitExceededException() throws Exception {
        User Pasha = new User((long) 1, "Batychko Pavel", new Wallet((long) 2, BigDecimal.valueOf(1234.50), WalletStatus.ACTIVE, BigDecimal.valueOf(100000.00)));
        Pasha.getWallet().checkTransfer(BigDecimal.valueOf(100001));
    }

    @Test
    public void testTransfer() throws Exception {
        User Pasha = new User((long) 1, "Batychko Pavel", new Wallet((long) 2, BigDecimal.valueOf(1234.50), WalletStatus.ACTIVE, BigDecimal.valueOf(100000.00)));
        Pasha.getWallet().transfer(BigDecimal.valueOf(2000));
        assertEquals(BigDecimal.valueOf(3234.50), Pasha.getWallet().getAmount());
    }
}
