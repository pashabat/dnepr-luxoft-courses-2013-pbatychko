package com.luxoft.dnepr.courses.regular.unit8;

import com.luxoft.dnepr.courses.regular.unit5.model.Employee;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class SerializerTest {

    public Person person = new Person();
    private String filePath = "d:\\luxoftSerialize.txt";

    @Before
    public void createPerson() {
        person.setBirthDate(new Date());
        person.setEthnicity("ukrainian");
        person.setGender(Gender.FEMALE);
        person.setName("Galja");
        Person father = new Person();
        father.setBirthDate(new Date());
        father.setEthnicity("ukrainian");
        father.setGender(Gender.MALE);
        father.setName("Vasiliy");
        person.setFather(father);
    }

    @Test
    public void testSerialize() throws Exception {
        FamilyTree obj = new FamilyTree(person);
        Serializer.serialize(new File(filePath), obj);
    }

    @Test
    public void testDeserialize() throws Exception {
        FamilyTree obj = Serializer.deserialize(new File(filePath));
        FamilyTree obj2 = new FamilyTree(person);
        assertEquals(obj.getRoot().getEthnicity(), obj2.getRoot().getEthnicity());
        assertEquals(obj.getRoot().getGender(), obj2.getRoot().getGender());
        assertEquals(obj.getRoot().getName(), obj2.getRoot().getName());
        assertEquals(obj.getRoot().getFather().getEthnicity(), obj2.getRoot().getFather().getEthnicity());
        assertEquals(obj.getRoot().getFather().getName(), obj2.getRoot().getFather().getName());
        assertEquals(obj.getRoot().getFather().getGender(), obj2.getRoot().getFather().getGender());
    }
}
