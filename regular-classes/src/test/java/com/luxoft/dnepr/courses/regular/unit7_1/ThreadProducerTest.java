package com.luxoft.dnepr.courses.regular.unit7_1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class ThreadProducerTest {
    @Test
    public void testGetNewThread() throws Exception {
        assertEquals(Thread.State.NEW, ThreadProducer.getNewThread().getState());
        assertEquals(Thread.State.RUNNABLE, ThreadProducer.getRunnableThread().getState());
        assertEquals(Thread.State.WAITING, ThreadProducer.getWaitingThread().getState());
        assertEquals(Thread.State.TIMED_WAITING, ThreadProducer.getTimedWaitingThread().getState());
        assertEquals(Thread.State.BLOCKED, ThreadProducer.getBlockedThread().getState());
        assertEquals(Thread.State.TERMINATED, ThreadProducer.getTerminatedThread().getState());
    }
}
