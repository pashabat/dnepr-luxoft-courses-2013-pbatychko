package com.luxoft.dnepr.courses.regular.unit18.controller;

import java.util.Vector;


public class Words {

    private static Vector<String> known = new Vector();
    private static Vector<String> unknown = new Vector();


    public static int res(int tot) {
        return (tot * (known.size() + 1) / (known.size() + unknown.size() + 1));
    }


    public static void answer(String word, Boolean answer) {
        known.add(answer ? word : "");
        unknown.add(!answer ? word : "");
    }

}
