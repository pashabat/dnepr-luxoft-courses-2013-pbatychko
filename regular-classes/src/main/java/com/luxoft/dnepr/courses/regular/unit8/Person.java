package com.luxoft.dnepr.courses.regular.unit8;

import com.google.gson.Gson;

import java.io.*;
import java.util.Date;


public class Person implements Externalizable {

    private String name;
    private Gender gender;
    private String ethnicity;
    private Date birthDate;
    private Person father;
    private Person mother;


    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        name = (String) in.readObject();
        String genderString = (String) in.readObject();
        for (Gender tmp : Gender.values()) {
            if (genderString.equals(tmp.toString())) gender = tmp;
        }
        ethnicity = (String) in.readObject();
        birthDate = (Date) in.readObject();
        father = (Person) in.readObject();
        mother = (Person) in.readObject();
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeObject(name);
        out.writeObject(gender);
        out.writeObject(ethnicity);
        out.writeObject(birthDate);
        out.writeObject(father);
        out.writeObject(mother);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getEthnicity() {
        return ethnicity;
    }

    public void setEthnicity(String ethnicity) {
        this.ethnicity = ethnicity;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Person getFather() {
        return father;
    }

    public void setFather(Person father) {
        this.father = father;
    }

    public Person getMother() {
        return mother;
    }

    public void setMother(Person mother) {
        this.mother = mother;
    }
}
