package com.luxoft.dnepr.courses.regular.unit8;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.Date;

public class FamilyTree implements Externalizable {

    private Person root;
    private transient Date creationTime;

    public FamilyTree() {
        setCreationTime(new Date());
    }

    public FamilyTree(Person root) {
        this();
        setRoot(root);
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        root = (Person) in.readObject();
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeObject(root);
    }

    public Person getRoot() {
        return root;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    private void setRoot(Person root) {
        this.root = root;
    }

    private void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }
}
