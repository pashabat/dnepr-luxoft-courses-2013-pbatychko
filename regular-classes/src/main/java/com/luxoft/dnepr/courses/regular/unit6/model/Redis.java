package com.luxoft.dnepr.courses.regular.unit6.model;


public class Redis extends Entity {

    private int weight;


    public int getWeight() {
        return weight;
    }


    public void setWeight(int weight) {
        this.weight = weight;
    }


    public Redis() {
    }


    public Redis(long id, int weight) {
        setId(id);
        this.weight = weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Redis redis = (Redis) o;

        if (weight != redis.weight) return false;
        if (getId() != redis.getId()) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = weight;
        result = 31 * result + (int) (getId() ^ (getId() >>> 32));
        return result;
    }
}
