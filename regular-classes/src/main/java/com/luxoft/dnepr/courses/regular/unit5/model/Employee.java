package com.luxoft.dnepr.courses.regular.unit5.model;


public class Employee extends Entity {

    private int salary;


    public int getSalary() {
        return salary;
    }


    public void setSalary(int salary) {
        this.salary = salary;
    }


    public Employee() {
    }


    public Employee(long id, int salary) {
        setId(id);
        this.salary = salary;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Employee employee = (Employee) o;

        if (salary != employee.salary) return false;
        if (getId() != employee.getId()) return false;

        return true;
    }


    @Override
    public int hashCode() {
        int result = salary;
        result = 31 * result + (int) (getId() ^ (getId() >>> 32));
        return result;
    }
}
