package com.luxoft.dnepr.courses.regular.unit4;


import java.util.*;


public class EqualSet<E> implements Set<E> {

    private Collection<E> equalArray;


    public EqualSet() {
        equalArray = new ArrayList<E>();
    }


    public EqualSet(Collection<? extends E> collection) {
        equalArray = new ArrayList<E>();
        if (collection != null && collection.size() >= 1) {
            for (E tmp : collection) equalArray.add(tmp);
        }
    }


    @Override
    public boolean add(E element) {
        if (element == null) {
            for (E tmp : equalArray) {
                if (tmp == null) return false;
            }
        }
        return equalArray.add(element);
    }


    @Override
    public int size() {
        return equalArray.size();
    }


    @Override
    public void clear() {
        equalArray.clear();
    }


    @Override
    public boolean isEmpty() {
        return equalArray.isEmpty();
    }


    @Override
    public boolean remove(Object object) {
        for (E i : equalArray) {
            if ((i == null && object == null) || (i != null && i.equals(object))) {
                return equalArray.remove(i);
            }
        }
        return false;
    }


    @Override
    public boolean removeAll(Collection<?> collection) {
        if (collection == null || collection.size() == 0) return false;
        boolean flag = false;

        for (Object object : collection) {
            if (equalArray.remove(object)) {
                flag = true;
            }
        }
        return flag;
    }


    @Override
    public boolean addAll(Collection<? extends E> c) {
        return equalArray.addAll(c);
    }


    @Override
    public boolean contains(Object element) {
        for (E tmp : equalArray) {
            if (tmp != null && tmp.equals(element)) return true;
            if (tmp == null && element == null) return true;
        }
        return false;
    }


    @Override
    public boolean containsAll(Collection<?> collection) {
        if (collection == null) return false;
        if (collection.size() == 0 && equalArray.size() == 0) return true;
        if (collection.size() == 0) return false;

        for (Object tmp : collection) {
            if (!equalArray.contains(tmp)) return false;
        }
        return true;
    }


    @Override
    public boolean retainAll(Collection<?> collection) {
        if (collection == null || equalArray.size() == 0 || collection.size() == 0) return false;

        Collection<E> newArray = new ArrayList<E>();

        for (E tmp : equalArray) {
            if (collection.contains(tmp)) newArray.add(tmp);
        }
        equalArray.clear();
        equalArray.addAll(newArray);

        return (newArray.size() != equalArray.size());
    }


    @Override
    public Object[] toArray() {
        Object[] tmp = new Object[equalArray.size()];
        int cont = 0;
        for (E i : equalArray) {
            tmp[cont] = i;
            cont++;
        }
        return tmp;
    }


    @Override
    public Iterator<E> iterator() {
        return equalArray.iterator();
    }


    @Override
    public <T> T[] toArray(T[] array) {
        return equalArray.toArray(array);
    }


    public boolean equals(Collection<E> collection) {
        if (collection == null || getClass() != collection.getClass() || equalArray.size() != collection.size())
            return false;
        if (equalArray == collection) return true;

        Iterator<E> iteratorThis = equalArray.iterator();
        Iterator<E> iteratorObject = collection.iterator();

        while (iteratorThis.hasNext()) {
            if (!iteratorThis.next().equals(iteratorObject.next())) return false;
        }

        return true;
    }


    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;

        EqualSet equalSet = (EqualSet) object;

        if (equalArray != null ? !equalArray.equals(equalSet.equalArray) : equalSet.equalArray != null) return false;

        return true;
    }


}
