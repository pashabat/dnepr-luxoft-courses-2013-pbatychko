package com.luxoft.dnepr.courses.regular.unit18.controller;

import java.io.*;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;


public class Vocabulary {

    private Set<String> voc = new HashSet<String>();


    public Vocabulary(String filePath) throws IOException {
        String fileContent;

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath))) {
            while ((fileContent = bufferedReader.readLine()) != null) {
                addWordsFromStringToVocabulary(fileContent);
            }
        }
    }


    public String random() {
        return voc.toArray(new String[]{})[(int) (Math.random() * voc.size())];
    }


    private void addWordsFromStringToVocabulary(String words) {
        StringTokenizer stringTokenizer = new StringTokenizer(words.toString());
        while (stringTokenizer.hasMoreTokens()) {
            String word = stringTokenizer.nextToken();
            if (word.length() > 3) voc.add(word.toLowerCase());
        }
    }


    public Set<String> getVoc() {
        return Collections.unmodifiableSet(voc);
    }
}
