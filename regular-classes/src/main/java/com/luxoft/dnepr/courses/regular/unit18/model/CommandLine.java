package com.luxoft.dnepr.courses.regular.unit18.model;


public interface CommandLine {
    String EXIT = "exit";
    String HELP = "help";
    String YES = "Y";
}