package com.luxoft.dnepr.courses.regular.unit6.dao;


import com.luxoft.dnepr.courses.regular.unit6.exception.EntityAlreadyExistException;
import com.luxoft.dnepr.courses.regular.unit6.exception.EntityNotFoundException;
import com.luxoft.dnepr.courses.regular.unit6.model.Entity;
import com.luxoft.dnepr.courses.regular.unit6.storage.EntityStorage;


public abstract class AbstractDao<E extends Entity> implements IDao<E> {


    public E save(E e) throws EntityAlreadyExistException {
        if (e == null) return e;
        synchronized (EntityStorage.getEntities()) {
            if ((Long) e.getId() == null) {
                e.setId(EntityStorage.getNextId());
            } else if (EntityStorage.getEntities().containsKey(e.getId())) {
                throw new EntityAlreadyExistException();
            }
            EntityStorage.getEntities().put(e.getId(), e);
        }
        return e;
    }


    public E update(E e) throws EntityNotFoundException {
        if ((Long) e.getId() == null) throw new EntityNotFoundException();
        synchronized (EntityStorage.getEntities()) {
            if(!EntityStorage.getEntities().containsKey(e.getId())) throw new EntityNotFoundException();
            EntityStorage.getEntities().put(e.getId(), e);
        }
        return e;
    }


    public E get(long id) {
        if ((Long) id != null) {
            E tmpE = (E) EntityStorage.getEntities().get(id);
            if (tmpE != null) return tmpE;
        }
        return null;
    }


    public boolean delete(long id) {
        synchronized (EntityStorage.getEntities()) {
            return EntityStorage.getEntities().remove(id) != null;
        }
    }


}