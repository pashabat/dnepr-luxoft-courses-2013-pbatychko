package com.luxoft.dnepr.courses.regular.unit2;

import java.util.Date;

public class Book extends AbstractProduct implements Cloneable {
    private Date publicationDate;

    public void setPublicationDate(Date publicationDate) {
        this.publicationDate = publicationDate;
    }

    public Date getPublicationDate() {
        return publicationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Book book = (Book) o;

        if (!publicationDate.equals(book.publicationDate)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + publicationDate.hashCode();
        return result;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        Book newBook = (Book) super.clone();
        newBook.setPublicationDate((Date) publicationDate.clone());
        return newBook;
    }
}
