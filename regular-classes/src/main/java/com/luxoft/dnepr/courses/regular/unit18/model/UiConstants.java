package com.luxoft.dnepr.courses.regular.unit18.model;


public interface UIConstants {
    String HELP_TEXT = "\nLinguistic analyzer v1, will check your english knowledge" +
            "\nfor help type \"" + CommandLine.HELP + "\"" +
            "\nfor yes answer type \"" + CommandLine.YES + "\"" +
            "\nfor exit type \"" + CommandLine.EXIT + "\"\n";
    String ANSWER_ERROR = "Error while reading answer";
    String QUESTION = "\nDo you know translation of the word \"%s\"?";
    String RESULT_TEXT = "Your estimated vocabulary is %s words";
    String FILE_TITLE = "sonnets.txt";
    String OPEN_FILE_ERROR_TEXT = "Open file error!";
}
