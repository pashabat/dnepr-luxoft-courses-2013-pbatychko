package com.luxoft.dnepr.courses.regular.unit5.storage;


import com.luxoft.dnepr.courses.regular.unit5.model.Entity;

import java.util.HashMap;
import java.util.Map;


public class EntityStorage {

    private static EntityStorage entityStorage = null;
    private final static Map<Long, Entity> entities = new HashMap();


    private EntityStorage() {
    }


    public static EntityStorage getInstance() {
        if (entityStorage == null) {
            entityStorage = new EntityStorage();
        }
        return entityStorage;
    }


    public static Map<Long, Entity> getEntities() {
        return entities;
    }

}
