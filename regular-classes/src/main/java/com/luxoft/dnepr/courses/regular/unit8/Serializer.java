package com.luxoft.dnepr.courses.regular.unit8;

import com.google.gson.Gson;

import java.io.*;


public class Serializer {

    public static void serialize(File file, FamilyTree entity) {
        Gson gson = new Gson();
        String json = gson.toJson(entity);
        try {
            FileWriter writer = new FileWriter(file);
            writer.write(json);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static FamilyTree deserialize(File file) {
        Gson gson = new Gson();
        FamilyTree familyTree;
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            familyTree = gson.fromJson(br, FamilyTree.class);
            return familyTree;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new FamilyTree();
    }
}