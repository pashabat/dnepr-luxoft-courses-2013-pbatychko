package com.luxoft.dnepr.courses.regular.unit3.exceptions;


import java.math.BigDecimal;

public class LimitExceededException extends Exception {

    private Long walletId;
    private BigDecimal amountToTransfer;
    private BigDecimal amountInWallet;
    private String message;


    public LimitExceededException(Long walletId, BigDecimal amountToTransfer, BigDecimal amountInWallet, String message) {
        this.walletId = walletId;
        this.amountToTransfer = amountToTransfer;
        this.amountInWallet = amountInWallet;
        this.message = message;
    }


    @Override
    public String toString() {
        return message;
    }


    public Long getWalletId() {
        return walletId;
    }


    public BigDecimal getAmountToTransfer() {
        return amountToTransfer;
    }


    public BigDecimal getAmountInWallet() {
        return amountInWallet;
    }
}
