package com.luxoft.dnepr.courses.compiler;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Compiler {

    public static final boolean DEBUG = true;
    public static final String ERROR_OPERAND = "error";

    public static void main(String[] args) {
        byte[] byteCode = compile(getInputString(args));
        VirtualMachine vm = VirtualMachineEmulator.create(byteCode, System.out);
        vm.run();
    }

    private static String splitForbiddenSymbols(String symbol) {
        if (symbol.trim() == "+") return "\\+";
        else if (symbol.trim() == "*") return "\\*";
        else return symbol;
    }

    public static String returnSign(String s) {
        if (s.indexOf("*") > 0) return "*";
        else if (s.indexOf("/") > 0) return "/";
        else if (s.indexOf("+") > 0) return "+";
        else if (s.indexOf("-") > 0) return "-";
        return ERROR_OPERAND;
    }

    public static double[] returnNumbers(String s) {
        try {
            String[] tmp = s.split(splitForbiddenSymbols(returnSign(s)));
            tmp[0] = tmp[0].trim();
            tmp[1] = tmp[1].trim();
            if (tmp.length == 2 && tmp[0].length() > 0 && tmp[1].length() > 0)
                return new double[]{Double.parseDouble(tmp[0]), Double.parseDouble(tmp[1])};
        } catch (Exception e) {
            return new double[]{0, 0};
        }
        return new double[]{0, 0};
    }

    static byte[] compile(String input) {
        ByteArrayOutputStream result = new ByteArrayOutputStream();

        String s = returnSign(input);

        if (input.equals(null) || input.equals("") || input.length() < 3 || s.equals(ERROR_OPERAND)) {
            addCommand(result, VirtualMachine.PUSH, 0);
            addCommand(result, VirtualMachine.PRINT);
            return result.toByteArray();
        }

        double[] mas = returnNumbers(input);

        double leftOperand = mas[0];
        double rightOperand = mas[1];

        if (s.equals("+")) {
            addCommand(result, VirtualMachine.PUSH, leftOperand);
            addCommand(result, VirtualMachine.PUSH, rightOperand);
            addCommand(result, VirtualMachine.ADD);
            addCommand(result, VirtualMachine.PRINT);
        }
        if (s.equals("-")) {
            addCommand(result, VirtualMachine.PUSH, rightOperand);
            addCommand(result, VirtualMachine.PUSH, leftOperand);
            addCommand(result, VirtualMachine.SUB);
            addCommand(result, VirtualMachine.PRINT);
        }
        if (s.equals("*")) {
            addCommand(result, VirtualMachine.PUSH, rightOperand);
            addCommand(result, VirtualMachine.PUSH, leftOperand);
            addCommand(result, VirtualMachine.MUL);
            addCommand(result, VirtualMachine.PRINT);
        }
        if (s.equals("/")) {
            if (rightOperand == 0) {
                leftOperand = 0;
                rightOperand = 1;
            }
            addCommand(result, VirtualMachine.PUSH, rightOperand);
            addCommand(result, VirtualMachine.PUSH, leftOperand);
            addCommand(result, VirtualMachine.DIV);
            addCommand(result, VirtualMachine.PRINT);

        }

        return result.toByteArray();
    }

    /**
     * Adds specific command to the byte stream.
     *
     * @param result
     * @param command
     */
    public static void addCommand(ByteArrayOutputStream result, byte command) {
        result.write(command);
    }

    /**
     * Adds specific command with double parameter to the byte stream.
     *
     * @param result
     * @param command
     * @param value
     */
    public static void addCommand(ByteArrayOutputStream result, byte command, double value) {
        result.write(command);
        writeDouble(result, value);
    }

    private static void writeDouble(ByteArrayOutputStream result, double val) {
        long bits = Double.doubleToLongBits(val);

        result.write((byte) (bits >>> 56));
        result.write((byte) (bits >>> 48));
        result.write((byte) (bits >>> 40));
        result.write((byte) (bits >>> 32));
        result.write((byte) (bits >>> 24));
        result.write((byte) (bits >>> 16));
        result.write((byte) (bits >>> 8));
        result.write((byte) (bits >>> 0));
    }

    private static String getInputString(String[] args) {
        if (args.length > 0) {
            return join(Arrays.asList(args));
        }

        Scanner scanner = new Scanner(System.in);
        List<String> data = new ArrayList<String>();
        while (scanner.hasNext()) {
            data.add(scanner.next());
        }
        return join(data);
    }

    private static String join(List<String> list) {
        StringBuilder result = new StringBuilder();
        for (String element : list) {
            result.append(element);
        }
        return result.toString();
    }

}
