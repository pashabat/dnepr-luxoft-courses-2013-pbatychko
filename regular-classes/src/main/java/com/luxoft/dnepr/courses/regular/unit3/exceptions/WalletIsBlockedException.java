package com.luxoft.dnepr.courses.regular.unit3.exceptions;


public class WalletIsBlockedException extends Exception {

    private Long walletId;
    private String message;


    public WalletIsBlockedException(Long walletId, String message) {
        this.walletId = walletId;
        this.message = message;
    }


    @Override
    public String toString() {
        return message;
    }


    public Long getWalletId() {
        return walletId;
    }
}
