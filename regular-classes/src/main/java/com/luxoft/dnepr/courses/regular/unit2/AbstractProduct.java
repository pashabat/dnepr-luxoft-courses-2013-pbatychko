package com.luxoft.dnepr.courses.regular.unit2;


public abstract class AbstractProduct implements Product, Cloneable {

    private String code, name;
    private double price;

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public void setCode(String tmp) {
        code = tmp;
    }

    public void setName(String tmp) {
        name = tmp;
    }

    public void setPrice(double tmp) {
        price = tmp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AbstractProduct that = (AbstractProduct) o;

        if (!code.equals(that.code)) return false;
        if (!name.equals(that.name)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = code.hashCode();
        result = 31 * result + name.hashCode();
        return result;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return (AbstractProduct) super.clone();
    }

}
