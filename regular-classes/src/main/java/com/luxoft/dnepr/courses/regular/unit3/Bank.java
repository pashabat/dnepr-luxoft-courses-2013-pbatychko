package com.luxoft.dnepr.courses.regular.unit3;


import com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.*;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Bank implements BankInterface {

    private Map<Long, UserInterface> users;


    public Bank(String expectedJavaVersion) throws IllegalJavaVersionError {
        if (!System.getProperty("java.version").equals(expectedJavaVersion)) {
            throw new IllegalJavaVersionError(System.getProperty("java.version"), expectedJavaVersion, "");
        }
    }


    public Map<Long, UserInterface> getUsers() {
        return Collections.unmodifiableMap(users);
    }


    public void setUsers(Map<Long, UserInterface> users) {
        this.users = new HashMap<Long, UserInterface>(users);
    }


    private UserInterface findUserById(long userId) throws NoUserFoundException {
        for (Map.Entry<Long, UserInterface> user : users.entrySet()) {
            if (user.getValue().getId().equals(userId)) {
                return user.getValue();
            }
        }
        throw new NoUserFoundException(userId, "");
    }


    private String WalletIsBlockedMessage(UserInterface user) {
        return "User " + user.getName() + " wallet is blocked";
    }


    private String InsufficientWalletAmountMessage(UserInterface user, BigDecimal amount) {
        return "User " + user.getName() + " has insufficient funds (" + user.getWallet().getAmount().toString() + " < " + amount.toString() + ")";
    }


    private String LimitExceededMessage(UserInterface user, BigDecimal amount) {
        return "User '" + user.getName() + "' wallet limit exceeded (" + user.getWallet().getAmount().toString() + " + " + amount.toString() + " > " + user.getWallet().getMaxAmount().toString() + ")";
    }


    public void makeMoneyTransaction(Long fromUserId, Long toUserId, BigDecimal amount) throws NoUserFoundException, TransactionException {

        UserInterface userFrom = findUserById(fromUserId);
        UserInterface userTo = findUserById(toUserId);

        try {

            userFrom.getWallet().checkWithdrawal(amount);

        } catch (WalletIsBlockedException e) {
            throw new TransactionException(WalletIsBlockedMessage(userFrom));
        } catch (InsufficientWalletAmountException e) {
            throw new TransactionException(InsufficientWalletAmountMessage(userFrom, amount));
        }


        try {

            userTo.getWallet().checkTransfer(amount);

        } catch (WalletIsBlockedException e) {
            throw new TransactionException(WalletIsBlockedMessage(userTo));
        } catch (LimitExceededException e) {
            throw new TransactionException(LimitExceededMessage(userTo, amount));
        }


        userFrom.getWallet().withdraw(amount);
        userTo.getWallet().transfer(amount);

    }


}
