package com.luxoft.dnepr.courses.regular.unit18.view;

import com.luxoft.dnepr.courses.regular.unit18.controller.Vocabulary;
import com.luxoft.dnepr.courses.regular.unit18.controller.Words;
import com.luxoft.dnepr.courses.regular.unit18.model.CommandLine;
import com.luxoft.dnepr.courses.regular.unit18.model.UIConstants;

import java.io.IOException;
import java.util.Scanner;


public class UI implements Runnable {

    private Vocabulary vocab;


    public void run() {

        try {
            vocab = new Vocabulary(UIConstants.FILE_TITLE);
        } catch (IOException e) {
            System.out.println(UIConstants.OPEN_FILE_ERROR_TEXT);
        }
        String enterLine;
        Scanner scanner = new Scanner(System.in);
        do {
            String word = vocab.random();
            System.out.println(String.format(UIConstants.QUESTION, word));

            enterLine = scanner.next();
            if (!execute(enterLine, word)) {
                return;
            }
        } while (enterLine != null);
    }


    private boolean execute(String enterLine, String word) {
        if (enterLine != null) {
            if (enterLine.equals(CommandLine.EXIT)) {
                System.out.println(String.format(UIConstants.RESULT_TEXT, Words.res(vocab.getVoc().size())));
                return false;
            } else if (enterLine.equals(CommandLine.HELP)) {
                System.out.println(UIConstants.HELP_TEXT);
                return true;
            }
            Boolean userAnswer = enterLine.equalsIgnoreCase(CommandLine.YES);
            Words.answer(word, userAnswer);
        } else {
            System.out.println(UIConstants.ANSWER_ERROR);
        }
        return true;
    }


    public static void main(String[] args) {
        UI ui = new UI();
        ui.run();
    }
}
