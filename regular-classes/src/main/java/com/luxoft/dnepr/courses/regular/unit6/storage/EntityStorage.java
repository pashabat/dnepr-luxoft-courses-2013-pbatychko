package com.luxoft.dnepr.courses.regular.unit6.storage;


import com.luxoft.dnepr.courses.regular.unit6.model.Entity;

import java.util.HashMap;
import java.util.Map;


public class EntityStorage {

    private final static Map<Long, Entity> entities = new HashMap();


    private EntityStorage() {
    }


    public static Map<Long, Entity> getEntities() {
        return entities;
    }


    // return maxId + 1
    public static Long getNextId() {
        long id = 0;
        for (long i : entities.keySet()) {
            if (i > id) id = i;
        }
        id++;
        return id;
    }

}
