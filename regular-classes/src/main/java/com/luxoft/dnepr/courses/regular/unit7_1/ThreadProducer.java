package com.luxoft.dnepr.courses.regular.unit7_1;


public class ThreadProducer {

    private static int sleepMillis = 1000;
    private static final Object lock = new Object();


    private static void sleep() {
        try {
            Thread.sleep(sleepMillis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public static Thread getNewThread() {
        Thread exampleThread = new Thread();
        return exampleThread;
    }


    public static Thread getRunnableThread() {
        Thread exampleThread = new Thread();
        exampleThread.start();
        return exampleThread;
    }


    public static Thread getBlockedThread() {

        final Object lock1 = new Object();
        final Object lock2 = new Object();

        Thread t1 = new Thread() {
            @Override
            public void run() {
                synchronized (lock1) {
                    try {
                        sleep(sleepMillis);
                    } catch (InterruptedException e) {
                    }
                    synchronized (lock2) {
                        try {
                            sleep(sleepMillis);
                        } catch (InterruptedException e) {
                        }
                    }
                }
            }
        };

        Thread t2 = new Thread() {
            @Override
            public void run() {
                synchronized (lock2) {
                    try {
                        sleep(sleepMillis);
                    } catch (InterruptedException e) {
                    }
                    synchronized (lock1) {
                        try {
                            sleep(sleepMillis);
                        } catch (InterruptedException e) {
                        }
                    }
                }
            }
        };

        t1.start();
        t2.start();
        while (!(t1.getState() == Thread.State.BLOCKED)) {
        }
        return t1;
    }


    public static Thread getWaitingThread() {
        Thread exampleThread = new Thread() {
            @Override
            public synchronized void run() {
                try {
                    wait();
                } catch (InterruptedException e) {
                }
            }
        };

        exampleThread.start();
        while (!(exampleThread.getState() == Thread.State.WAITING)) {
        }
        return exampleThread;
    }


    public static Thread getTimedWaitingThread() {
        Thread exampleThread = new Thread() {
            @Override
            public void run() {
                try {
                    sleep(100);
                } catch (InterruptedException e) {
                }
            }
        };

        exampleThread.start();
        while (!(exampleThread.getState() == Thread.State.TIMED_WAITING)) {
        }
        return exampleThread;
    }


    public static Thread getTerminatedThread() {
        Thread exampleThread = new Thread(new Runnable() {
            @Override
            public void run() {
            }
        });
        exampleThread.start();
        while (!(exampleThread.getState() == Thread.State.TERMINATED)) {
        }
        return exampleThread;
    }

}
