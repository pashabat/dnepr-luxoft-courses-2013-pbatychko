package com.luxoft.dnepr.courses.regular.unit5.dao;


import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Entity;
import com.luxoft.dnepr.courses.regular.unit5.storage.EntityStorage;

import java.util.Map;


public abstract class AbstractDao<E extends Entity> implements IDao<E> {

    protected Map<Long, Entity> tmpStorage = EntityStorage.getInstance().getEntities();


    // return maxId + 1
    protected long nextId() {
        long id = 0;
        for (long i : tmpStorage.keySet()) {
            if (i > id) id = i;
        }
        id++;
        return id;
    }


    public E save(E e) throws UserAlreadyExist {
        if (e == null) {
            return e;
        } else if ((Long) e.getId() == null) {
            e.setId(nextId());
        } else if (tmpStorage.containsKey(e.getId())) {
            throw new UserAlreadyExist();
        }
        tmpStorage.put(e.getId(), e);
        return e;
    }


    public E update(E e) throws UserNotFound {
        if ((Long) e.getId() == null || !tmpStorage.containsKey(e.getId())) throw new UserNotFound();
        tmpStorage.put(e.getId(), e);
        return e;
    }


    public E get(long id) {
        if ((Long) id != null) {
            E tmpE = (E) tmpStorage.get(id);
            if (tmpE != null) return tmpE;
        }
        return null;
    }


    public boolean delete(long id) {
        return tmpStorage.remove(id) != null;
    }


}