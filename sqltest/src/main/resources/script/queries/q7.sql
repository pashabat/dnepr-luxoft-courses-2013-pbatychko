SELECT DISTINCT makers.maker_name
FROM makers
LEFT JOIN product ON makers.maker_id = product.maker_id
LEFT JOIN pc ON product.model = pc.model
WHERE pc.speed >= 450