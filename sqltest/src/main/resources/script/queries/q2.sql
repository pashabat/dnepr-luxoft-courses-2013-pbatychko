SELECT speed, makers.maker_name
FROM laptop
LEFT JOIN product ON laptop.model = product.model
LEFT JOIN makers ON product.maker_id = makers.maker_id
WHERE hd >= 10