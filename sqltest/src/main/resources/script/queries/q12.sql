SELECT tmp.hd
FROM (
  SELECT  hd, COUNT(hd) AS count
  FROM pc
  GROUP BY hd
) AS tmp
WHERE tmp.count <= 2
ORDER BY tmp.hd DESC