SELECT DISTINCT maker_name
FROM makers
INNER JOIN product ON makers.maker_id = product.maker_id
INNER JOIN printer ON product.model = printer.model
ORDER BY maker_name DESC;