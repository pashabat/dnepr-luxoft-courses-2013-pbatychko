SELECT makers.maker_name, MIN(printer.price) AS min_price
FROM printer
LEFT JOIN product ON printer.model = product.model
LEFT JOIN makers ON product.maker_id = makers.maker_id
WHERE printer.color = 'y'