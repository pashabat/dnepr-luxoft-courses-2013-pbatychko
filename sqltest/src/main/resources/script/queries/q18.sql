SELECT MAX(price)
FROM (
	SELECT MAX(price) AS price, product.model
	FROM product
	INNER JOIN pc ON pc.model = product.model
	GROUP BY product.model

	UNION

	SELECT MAX(price) AS price, product.model
	FROM product
	INNER JOIN laptop ON laptop.model = product.model
	GROUP BY product.model

	UNION

	SELECT MAX(price) AS price, product.model
	FROM product
	INNER JOIN printer ON printer.model = product.model
	GROUP BY product.model
) AS all_products