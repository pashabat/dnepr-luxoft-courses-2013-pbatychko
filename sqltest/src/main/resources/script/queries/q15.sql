SELECT makers.maker_id, AVG(laptop.screen) AS avg_size
FROM makers
INNER JOIN product ON makers.maker_id = product.maker_id
INNER JOIN laptop ON product.model = laptop.model
GROUP BY makers.maker_id
ORDER BY avg_size ASC